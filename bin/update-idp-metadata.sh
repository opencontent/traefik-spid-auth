#!/usr/bin/env bash

[[ $DEBUG ]] && set -x

script_name=`basename "$0"`
if [[ $# == 0 ]]; then 
     echo "Call syntax: $script_name <target_dir>" 
     exit 
fi
target=$1
if [[ ! -d "$target" ]]; then
    echo "$target does not exists on your filesystem."
	exit
fi
idp_list=$(curl -s 'https://registry.spid.gov.it/assets/data/idp.json')
for k in $(jq '.data | keys | .[]' <<< "$idp_list"); do
	idp=$(jq ".data[$k]" <<< "$idp_list");
	code=$(jq -r '.ipa_entity_code' <<< "$idp");
	url=$(jq -r '.metadata_url' <<< "$idp");
	echo "Write $url in ${target}/${code}.xml"
	curl -s $url -o ${target}/${code}.xml
done

