# update metadata of IDP
FROM apteno/alpine-jq:2021-02-21

RUN apk add --no-cache bash

WORKDIR /srv

COPY ./bin/update-idp-metadata.sh /bin/update-idp-metadata

RUN mkdir metadata

RUN update-idp-metadata metadata

FROM abiosoft/caddy:1.0.3-php

COPY Caddyfile /etc/Caddyfile

WORKDIR /srv

RUN sed -i 's/;catch_workers_output = yes/catch_workers_output = yes/' /etc/php7/php-fpm.d/www.conf
RUN sed -i 's#;error_log = .*#error_log = /proc/self/fd/2#' /etc/php7/php-fpm.conf


COPY . ./ 
COPY ./app/config/parameters.yml.dist ./app/config/parameters.yml 
RUN composer install && \
    chown www-user var -R && \
    chown www-user ./app/config/parameters.yml

COPY --from=0 /srv/metadata/* spid/idp_metadata/

#bin/console cache:warmup    <- no, perché lo fai come root!

ADD ./docker-entrypoint.sh /

ENTRYPOINT [ "/docker-entrypoint.sh" ]
