<?php

namespace AppBundle\Services;

use Italia\Spid\Sp;

class Spid extends Sp
{
    public function __construct(array $settings)
    {
        $settings = $this->mergeEnvSettings($settings);
        parent::__construct($settings);
    }

    private function mergeEnvSettings($settings)
    {
        $additionalServiceIndex = getenv('SP_ADDITIONAL_SERVICES_INDEX');
        if ($additionalServiceIndex !== false){
            $additionalServiceIndexes = explode(',', $additionalServiceIndex);
            $currentIndex = (int)getenv('SP_CURRENT_INDEX');

            $settings['sp_assertionconsumerservice'][$currentIndex] = getenv('SP_ACS');
            $currentAttributesEnv = getenv('SP_ATTRIBUTES');
            if ($currentAttributesEnv !== false){
                $currentAttributes = explode(',', $currentAttributesEnv);
            }else {
                $currentAttributes = [
                    'registeredOffice',
                    'spidCode',
                    'name',
                    'companyName',
                    'ivaCode',
                    'countyOfBirth',
                    'idCard',
                    'gender',
                    'dateOfBirth',
                    'placeOfBirth',
                    'familyName',
                    'fiscalNumber',
                    'digitalAddress',
                    'mobilePhone',
                    'expirationDate',
                    'email',
                    'address',
                ];
            }
            $settings['sp_attributeconsumingservice'][$currentIndex] = $currentAttributes;
            $settings['sp_singlelogoutservice'][$currentIndex] = [getenv('SP_SINGLELOGOUT'), ''];

            $settings['additional_cert_files'] = [];
            foreach ($additionalServiceIndexes as $index){
                $acs = (string)getenv('SP_ADDITIONAL_SERVICE_' . $index . '_ACS');
                $attributes = explode(',', (string)getenv('SP_ADDITIONAL_SERVICE_' . $index . '_ATTRIBUTES'));
                $logout = (string)getenv('SP_ADDITIONAL_SERVICE_' . $index . '_LOGOUT');

                $settings['sp_assertionconsumerservice'][$index] = $acs;

                $settings['sp_attributeconsumingservice'][$index] = $attributes;

                $settings['additional_cert_files'][] = (string)getenv('SP_ADDITIONAL_SERVICE_' . $index . '_CERT_FILE');

                if (!empty($logout)){
                    if (strpos($logout, 'POST:') !== false){
                        $slo = str_replace('POST:', '', $logout);
                        $type = '';
                    }elseif (strpos($logout, 'REDIRECT:') !== false){
                        $slo = str_replace('REDIRECT:', '', $logout);
                        $type = 'REDIRECT';
                    }else{
                        $slo = $logout;
                        $type = '';
                    }
                    $settings['sp_singlelogoutservice'][$index] = [$slo, $type];
                }
            }

            ksort($settings['sp_assertionconsumerservice']);
            ksort($settings['sp_attributeconsumingservice']);
            ksort($settings['sp_singlelogoutservice']);
        }

        return $settings;
    }
}