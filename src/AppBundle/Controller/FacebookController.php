<?php

namespace AppBundle\Controller;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Helpers\AuthHelper;
use Psr\Log\LoggerInterface;

class FacebookController extends Controller
{
    /**
     * @var FilesystemCache
     */
    private $cache;

    /**
     * @var AuthHelper
     */
    private $helper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(AuthHelper $helper, LoggerInterface $logger)
    {
        $this->cache = new FilesystemCache();
        $this->helper = $helper;
        $this->logger = $logger;
    }

    /**
     * @Route("/connect/facebook", name="connect_facebook_start")
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('facebook') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect([
                'profile', 'email'
            ], []);
    }

    /**
     * @Route("/connect/facebook/check", name="connect_facebook_check")
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry)
    {
        /** @var \KnpU\OAuth2ClientBundle\Client\Provider\FacebookClient $client */
        $client = $clientRegistry->getClient('facebook');

        try {
            // the exact class depends on which provider you're using
            /** @var \League\OAuth2\Client\Provider\FacebookUser $user */
            $user = $client->fetchUser();

            $headers = [];
            $headers['X-Forwarded-User'] = $user->getId();
            $headers['X-Forwarded-User-Email'] = $user->getEmail();
            $headers['X-Forwarded-User-Familyname'] = $user->getLastName();
            $headers['X-Forwarded-User-Name'] = $user->getFirstName();
            $headers['X-Forwarded-User-Provider'] = AuthHelper::PROVIDER_FACEBOOK;

            $sessionId = md5($this->get('session')->getId());
            $this->cache->set($sessionId, json_encode($headers));
            return new RedirectResponse(
                $this->helper->getTarget() . '?_forward_sess=' . $sessionId,
                302
            );
        } catch (IdentityProviderException $e) {
            return new Response('Error: ' . $e->getMessage(), 500);
        }
    }
}
