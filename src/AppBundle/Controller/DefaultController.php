<?php

namespace AppBundle\Controller;

use AppBundle\EventSubscriber\OpenLoginRequestListener;
use AppBundle\Helpers\AuthHelper;
use Italia\Spid\Sp;
use Italia\Spid\Spid\Saml;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\InvalidArgumentException as InvalidArgumentExceptionAlias;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Cache\Simple\FilesystemCache;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @var Sp|Saml
     */
    private $sp;

    /**
     * @var AuthHelper
     */
    private $helper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var FilesystemCache
     */
    private $cache;

    /**
     * DefaultController constructor.
     * @param Sp $sp
     * @param AuthHelper $helper
     * @param LoggerInterface $logger
     */
    public function __construct(Sp $sp, AuthHelper $helper, LoggerInterface $logger)
    {
        $this->sp = $sp;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->cache = new FilesystemCache();
    }

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws InvalidArgumentExceptionAlias
     */
    public function indexAction(Request $request)
    {
        $this->logger->debug("Authenticating request");

        $this->helper->setSecret($this->container->getParameter('secret'));
        $this->helper->setCookieLifetimeSeconds($this->container->getParameter('cookie_lifetime_seconds'));
        $this->helper->setRequest($request);

        if (!$this->helper->hasTarget()) {
            $this->logger->debug($this->getTargetNotFoundErrorText());
            return new Response($this->getTargetNotFoundErrorText(true), 401);
        }

        if ($this->helper->hasAbort()) {
            return new Response('Abort', 200);
        }

        if ($this->helper->hasForwardSession()) {
            if ($this->helper->validateCSRFCookie() === AuthHelper::COOKIE_INVALID) {
                $this->logger->debug("Invalid CSRF cookie");

                return new Response('Unauthorized', 401);
            }
            $this->helper->clearCSRFCookie();

            $sessionId = $this->helper->getForwardSession();
            if ($this->cache->has($sessionId)) {
                $headers = json_decode($this->cache->get($sessionId), true);
                $this->cache->delete($sessionId);
                $this->logger->debug("Create auth cookie");
                $this->helper->createAuthCookie($headers);

                return new RedirectResponse($this->helper->getTarget(), 302);
            }
        }

        if ($this->helper->hasAuthCookie()) {
            $authCookieValidation = $this->helper->validateAuthCookie();
            $headers = $this->helper->getHeadersFromAuthCookie();
            $this->helper->clearAuthCookie();

            if ($headers && $authCookieValidation === AuthHelper::COOKIE_VALID) {
                return $this->getAuthorizedResponse($headers);
            }

            if ($authCookieValidation === AuthHelper::COOKIE_INVALID) {
                $this->logger->debug("Invalid auth cookie");

                return new Response('Unauthorized', 401);
            }

            if ($authCookieValidation === AuthHelper::COOKIE_EXPIRED) {
                $this->logger->debug("Auth cookie has expired");
            }
        }

        if (!$this->helper->hasProvider() || ($this->helper->getTarget() == '/' && !$this->helper->isAuthenticated())) {
            return $this->redirect('/provider?target=' . $this->helper->getTarget());
        }

        if (!$this->helper->isAuthenticated()) {
            if ($this->helper->validateCSRFCookie() === AuthHelper::COOKIE_NOT_SET) {
                $this->logger->debug("Create CSRF cookie");
                $this->helper->createCSRFCookie();
            }

            $this->helper->clearAuthCookie();
            return $this->redirect($this->helper->getAuthenticationRedirectUrl());
        }

        return $this->getAuthorizedResponse($this->getUserHeaders());
    }

    private function getAuthorizedResponse($headers)
    {
        $data = [];
        foreach ($headers as $key => $value){
            if (strpos($key, 'X-Forwarded-User-') !== false){
                $data[strtolower(str_replace('X-Forwarded-User-', '', $key))] = $value;
            }
        }
        $content = $this->container->get('twig')->render('@App/Default/authorized.html.twig', ['data' => $data]);
        return new Response($content, 200, $headers);
    }

    private function getUserHeaders()
    {
        $headers = [];
        foreach ($this->sp->getAttributes() as $key => $value) {
            if ($key === 'fiscalNumber') {
                $headers['X-Forwarded-User'] = str_replace(["\r\n", "\r", "\n"], ' ', $value);
            }
            $headers['X-Forwarded-User-' . $key] = str_replace(["\r\n", "\r", "\n"], ' ', $value);
        }
        $headers['X-Forwarded-User-Provider'] = AuthHelper::PROVIDER_SPID;
        $headers['X-Forwarded-User-Session'] = $_SESSION['spidSession']['sessionID'];
        $headers['X-Forwarded-User-Spid-Level'] = $_SESSION['spidSession']['level'];
        $headers['X-Forwarded-User-Spid-Idp'] = $_SESSION['spidSession']['idpEntityID'];

        return $headers;
    }

    /**
     * @Route("/provider", name="choose_provider", methods="GET")
     * @return Response
     */
    public function chooseProviderAction()
    {
        return $this->render('@App/Default/choose_provider.html.twig', ['providers' => $this->helper->getProviderList()]);
    }

    /**
     * @Route("/metadata", name="spid_metadata", methods="GET")
     * @return Response
     */
    public function metadataAction()
    {
        $response = new Response($this->sp->getSPMetadata());
        $response->headers->set('Content-Type', 'text/xml');
        return $response;
    }

    /**
     * @Route("/idp", name="spid_chooseidp", methods="GET")
     * @return Response
     */
    public function chooseIdpAction()
    {
        return $this->render('@App/Default/choose_idp.html.twig');
    }

    /**
     * @Route("/login", name="spid_login", methods="GET")
     * @param Request $request
     * @return Response
     */
    public function loginAction(Request $request)
    {
        $idp = $request->query->get('idp');
        if (!$idp) {
            throw new BadRequestHttpException('Missing the idp parameter');
        }

        $spidLevel = $this->container->hasParameter('spid_level') ? (int)$this->getParameter('spid_level') : 1;
        if ($request->query->has('level')){
            $spidLevelRequest = (int)$request->query->get('level');
            if ($spidLevelRequest >= 1 && $spidLevelRequest <= 3){
                $spidLevel = $spidLevelRequest;
            }
        }
        $redirectTo = "https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $encryptedRedirectTo = base64_encode(openssl_encrypt($redirectTo, "aes-256-cbc", $this->container->getParameter('secret')));

        if (getenv('SP_CURRENT_INDEX') !== false){
            $assertId = $attrId = (int)getenv('SP_CURRENT_INDEX');
        }else {
            $assertId = (int)getenv('SPID_LOGIN_ASSERTION_INDEX');
            $attrId = (int)getenv('SPID_LOGIN_ATTRIBUTE_INDEX');
        }

        $this->logger->debug("Login with SPID_LOGIN_ASSERTION_INDEX: $assertId and SPID_LOGIN_ATTRIBUTE_INDEX: $attrId");
        if (!@$this->sp->login($idp, $assertId, $attrId, $spidLevel, $encryptedRedirectTo)) {
            return $this->redirect('/');
        }

        throw new RuntimeException('Missing login redirect');
    }

    /**
     * @Route("/acs", name="spid_acs", methods="POST")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws InvalidArgumentExceptionAlias
     */
    public function acsAction(Request $request)
    {
        return $this->authAction($request);
    }

    /**
     * @Route("/auth", name="auth")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws InvalidArgumentExceptionAlias
     */
    public function authAction(Request $request)
    {
        /** @var SessionInterface $session */
        $session = $this->get('session');
        $listener = new OpenLoginRequestListener($session, $this->logger);
        $listener->setTarget($request);

        if (!$this->helper->hasTarget()) {
            $this->logger->debug($this->getTargetNotFoundErrorText());
            return new Response($this->getTargetNotFoundErrorText(true), 500);
        }

        if ($this->sp->isAuthenticated()) {
            $headers = $this->getUserHeaders();
            $this->cache->set($_SESSION['spidSession']['sessionID'], json_encode($headers));
            return new RedirectResponse(
                $this->helper->getTarget() . '?_forward_sess=' . $_SESSION['spidSession']['sessionID'],
                302
            );
        }

        return new RedirectResponse(
            $this->helper->getTarget() . '?_abort',
            302
        );
    }

    /**
     * @Route("/slo", name="spid_slo", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function singleLogoutAction(Request $request)
    {
        if (!$this->get('session')->has('logout_redirect')) {
            $redirect = '/';
            if ($request->get('redirect')) {
                $redirect = $request->get('redirect');
            } elseif ($request->headers->has('referer')) {
                $redirect = $request->headers->get('referer');
            }
            $this->get('session')->set('logout_redirect', $redirect);
        }

        if (getenv('SP_CURRENT_INDEX') !== false){
            $sloId = (int)getenv('SP_CURRENT_INDEX');
        }else {
            $sloId = (int)getenv('SPID_LOGOUT_INDEX');
        }

        if ($request->getMethod() == 'POST') {
            $this->sp->logoutPost($sloId);
        } else {
            $this->sp->logout($sloId);
        }

        $redirect = $this->get('session')->has('logout_redirect') ? $this->get('session')->get('logout_redirect') : '/';
        $this->get('session')->remove('logout_redirect');

        return $this->redirect($redirect);
    }

    /**
     * @Route("/health", name="health_check")
     * @return Response
     */
    public function healthAction()
    {
        return new Response('', 200);
    }

    private function getTargetNotFoundErrorText($asHtml = false)
    {
        return '<pre>Request header x-forwarded-uri empty or not found</pre>';
    }
}
