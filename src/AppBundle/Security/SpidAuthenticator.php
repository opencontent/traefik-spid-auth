<?php

namespace AppBundle\Security;

use AppBundle\EventSubscriber\OpenLoginRequestListener;
use Italia\Spid\Sp;
use Italia\Spid\Spid\Saml;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

/**
 * Class SpidAuthenticator
 * @property RouterInterface $router
 */
class SpidAuthenticator extends AbstractGuardAuthenticator
{

    /**
     * @var Sp|Saml
     */
    private $sp;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var LoggerInterface
     */
    private $logger;


    public function __construct(RouterInterface $router, Sp $sp, SessionInterface $session, LoggerInterface $logger)
    {
        $this->router = $router;
        $this->sp = $sp;
        $this->session = $session;
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $listener = new OpenLoginRequestListener($this->session, $this->logger);
        $listener->setTarget($request);

        return new RedirectResponse($this->router->generate('spid_chooseidp'));
    }

    /**
     * @param Request $request
     * @return array|null
     */
    public function getCredentials(Request $request)
    {
        $this->sp->isAuthenticated();
        return $_SESSION['spidSession'];
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     *
     * @return UserInterface
     * @throws \InvalidArgumentException
     */
    public function getUser($credentials, UserProviderInterface $userProvider): UserInterface
    {
        return $userProvider->loadUserByUsername($credentials['attributes']['fiscalNumber']);
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return Response
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, 403);
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /**
     * @inheritdoc
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }

    public function supports(Request $request): bool
    {
        if (!isset($_SESSION['spidSession']) ||
            !isset($_SESSION['spidSession']['idp']) ||
            !isset($_SESSION['spidSession']['level']) ||
            !isset($_SESSION['spidSession']['attributes'])
        ) {
            return false;
        } else {
            return true;
        }
    }
}
