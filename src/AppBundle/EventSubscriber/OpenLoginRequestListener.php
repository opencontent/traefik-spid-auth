<?php
namespace AppBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Psr\Log\LoggerInterface;

class OpenLoginRequestListener implements EventSubscriberInterface
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(SessionInterface $session, LoggerInterface $logger)
    {
        $this->session = $session;
        $this->logger = $logger;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => 'onKernelRequest'
        ];
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$this->session->isStarted()){
            $this->session->start();
        }
        if (!$event instanceof GetResponseEvent || !$event->isMasterRequest()) {
            return;
        }
        $request = $event->getRequest();
        $this->setTarget($request);
        $this->setForwardSession($request);
        $this->setProvider($request);
        $this->setAbort($request);
    }

    public function setTarget(Request $request)
    {
        $target = false;
        if ($request->get('target')) {
            $target = $request->get('target');
            $this->logger->debug('Found target in get');
//
//        } elseif ($request->headers->has('referer')) {
//            $target = $request->headers->get('referer');
//            $target = explode('?', $target)[0];
//            $this->logger->debug('Found target in referer header');

        } elseif ($request->headers->get('x-forwarded-host') != $request->headers->get('host')) {
            $target = $request->headers->get('x-forwarded-proto')
                . '://' . $request->headers->get('x-forwarded-host')
                . $request->headers->get('x-forwarded-uri');

            $target = explode('?', $target)[0];
            $this->logger->debug('Found target in x-forwarded-* headers');
        }

        if ($target) {
            $request->headers->set('X-OpenLogin-Target', $target);
            $this->session->set('target', $target);
            $this->logger->debug('Set target in session');
        }else{
            $this->logger->error('Target not found in request', [
                'target_in_session'=> $this->session->get('target'),
                'request' => $request]
            );
        }
    }

    public function setForwardSession(Request $request)
    {
        if ($request->headers->get('x-forwarded-uri')) {
            $forwardedUriHeader = $request->headers->get('x-forwarded-uri');
            $forwardedUriPos = strpos($forwardedUriHeader, '_forward_sess');
            if ($forwardedUriPos !== false) {
                $parts = explode('&', substr($forwardedUriHeader, $forwardedUriPos + strlen('_forward_sess=')));
                $sessionId = $parts[0];
                $request->headers->set('X-OpenLogin-ForwardSession', $sessionId);
            }
        }
    }

    public function setProvider(Request $request)
    {
        $provider = false;

        if ($request->headers->get('x-forwarded-uri')) {
            $forwardedUriHeader = $request->headers->get('x-forwarded-uri');
            $forwardedUriPos = strpos($forwardedUriHeader, '_provider');
            if ($forwardedUriPos !== false) {
                $parts = explode('&', substr($forwardedUriHeader, $forwardedUriPos + strlen('_provider=')));
                $provider = $parts[0];
            }
        }

        if ($provider) {
            $request->headers->set('X-OpenLogin-Provider', $provider);
        }
    }

    public function setAbort(Request $request)
    {
        if ($request->headers->get('x-forwarded-uri')) {
            $forwardedUriHeader = $request->headers->get('x-forwarded-uri');
            $abort = strpos($forwardedUriHeader, '_abort');
            if ($abort !== false) {
                $request->headers->set('X-OpenLogin-Abort', true);
            }
        }
    }
}
