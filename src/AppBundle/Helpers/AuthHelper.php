<?php

namespace AppBundle\Helpers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Italia\Spid\Sp;
use Italia\Spid\Spid\Saml;
use Psr\Log\LoggerInterface;

class AuthHelper
{
    const AUTH_COOKIE_NAME = '_forward_auth';

    const SESSION_COOKIE_NAME = '_forward_auth_csrf';

    const COOKIE_NOT_SET = 0;

    const COOKIE_VALID = 1;

    const COOKIE_EXPIRED = 2;

    const COOKIE_INVALID = 3;

    const PROVIDER_SPID = 'spid';

    const PROVIDER_GOOGLE = 'google';

    const PROVIDER_FACEBOOK = 'facebook';

    const PROVIDER_GITHUB = 'github';

    const PROVIDER_INSTAGRAM = 'instagram';

    /**
     * @var Request
     */
    private $request;

    private $secret = 'thiisnotsecret';

    private $cookieLifetimeSeconds = 10;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var Sp|Saml
     */
    private $sp;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(SessionInterface $session, Sp $sp, LoggerInterface $logger)
    {
        $this->session = $session;
        $this->sp = $sp;
        $this->logger = $logger;
    }

    public function setRequest(Request $request)
    {
        $this->request = $request;
    }

    public function setSecret($secret)
    {
        $this->secret = $secret;
    }

    public function setCookieLifetimeSeconds($cookieLifetimeSeconds)
    {
        $this->cookieLifetimeSeconds = $cookieLifetimeSeconds;
    }

    public function createAuthCookie($userData = [])
    {
        $data = json_encode($userData);
        $expires = time() + $this->cookieLifetimeSeconds;
        $mac = $this->cookieSignature($data, $expires);
        $value = sprintf("%s|%d|%s", $mac, $expires, $data);

        setcookie(self::AUTH_COOKIE_NAME . $this->getProvider(), $value, $expires);
    }

    private function cookieSignature($data, $expire)
    {
        return base64_encode(hash_hmac('sha256', $data . $expire, $this->secret));
    }

    public function hasAuthCookie()
    {
        return $this->request->cookies->get(self::AUTH_COOKIE_NAME . $this->getProvider());
    }

    public function validateAuthCookie()
    {
        $cookie = $this->request->cookies->get(self::AUTH_COOKIE_NAME . $this->getProvider());

        if (!$cookie) {
            return self::COOKIE_NOT_SET;
        }

        $parts = explode('|', $cookie, 3);
        if (count($parts) !== 3) {
            return self::COOKIE_INVALID;
        }

        $expectedSignature = $this->cookieSignature($parts[2], $parts[1]);

        if ($parts[0] != $expectedSignature) {
            return self::COOKIE_INVALID;
        }

        $now = time();
        if ($parts[1] < $now) {
            return self::COOKIE_EXPIRED;
        }

        return self::COOKIE_VALID;
    }

    public function getHeadersFromAuthCookie()
    {
        $cookie = $this->request->cookies->get(self::AUTH_COOKIE_NAME . $this->getProvider());
        $parts = explode('|', $cookie, 3);

        if (isset($parts[2])) {
            $headers = json_decode($parts[2], true);
            array_walk($headers, function (&$value, $index){
                $value = preg_replace("/\r\n|\r|\n/", ' ', $value);
            });

            return $headers;
        }

        return null;
    }

    public function clearAuthCookie()
    {
        setcookie(self::AUTH_COOKIE_NAME . $this->getProvider(), '', time() - 3600, '/');
    }

    public function createCSRFCookie()
    {
        $data = $this->getTarget();
        $expires = time() + $this->cookieLifetimeSeconds;
        $mac = $this->cookieSignature($data, $expires);
        $value = sprintf("%s|%d|%s", $mac, $expires, $data);

        setcookie(self::SESSION_COOKIE_NAME, $value, $expires, '/');
    }

    public function validateCSRFCookie()
    {
        if (!$this->request->cookies->has(self::SESSION_COOKIE_NAME)) {
            return self::COOKIE_NOT_SET;
        }

        $cookie = $this->request->cookies->get(self::SESSION_COOKIE_NAME);

        if (!$cookie) {
            return self::COOKIE_NOT_SET;
        }

        $parts = explode('|', $cookie, 3);
        if (count($parts) !== 3) {
            return self::COOKIE_INVALID;
        }

        $expectedSignature = $this->cookieSignature($parts[2], $parts[1]);

        if ($parts[0] != $expectedSignature) {
            return self::COOKIE_INVALID;
        }

        if ($parts[2] != $this->getTarget()) {
            return self::COOKIE_INVALID;
        }

        $now = time();
        if ($parts[1] < $now) {
            return self::COOKIE_EXPIRED;
        }

        return self::COOKIE_VALID;
    }

    public function clearCSRFCookie()
    {
        setcookie(self::SESSION_COOKIE_NAME, '', time() - 3600);
    }

    public function getTarget()
    {
        if ($this->session->has('target')) {
            return $this->session->get('target');
        }

        return $this->request && $this->request->headers->has('x-openlogin-target') ?
            $this->request->headers->get('x-openlogin-target') :
            '/';
    }

    public function hasTarget()
    {
        return $this->getTarget() !== null;
    }

    public function hasForwardSession()
    {
        return $this->request->headers->has('x-openlogin-forwardsession');
    }

    public function hasAbort()
    {
        return $this->request->headers->has('x-openlogin-abort');
    }

    public function getForwardSession()
    {
        return $this->request->headers->get('x-openlogin-forwardsession');
    }

    public function hasProvider()
    {
        if (count($this->getProviderList()) == 1) {
            return true;
        }

        return $this->request->headers->has('x-openlogin-provider');
    }

    public function getProvider()
    {
        if (count($this->getProviderList()) == 1) {
            return $this->getProviderList()[0]['identifier'];
        }
        return $this->request->headers->get('x-openlogin-provider');
    }

    public function isAuthenticated()
    {
        switch ($this->getProvider()) {
            case self::PROVIDER_SPID:
                return $this->sp->isAuthenticated();
                break;
        }

        return false;
    }

    public function getAuthenticationRedirectUrl()
    {
        foreach ($this->getProviderList() as $provider) {
            if ($provider['identifier'] == $this->getProvider()) {
                return $provider['redirect'];
            }
        }

        throw new \InvalidArgumentException("Redirect url for provider {$this->getProvider()} not found");
    }

    public function getProviderList()
    {
        $providers = [];
        $enabled = explode('|', getenv('ENABLE_PROVIDERS'));

        if (in_array(self::PROVIDER_SPID, $enabled)) {
            $providers[] = [
                'identifier' => self::PROVIDER_SPID,
                'name' => 'SPID',
                'redirect' => '/auth?target=' . $this->getTarget(),
                'icon' => null
            ];
        }
        if (in_array(self::PROVIDER_GOOGLE, $enabled)) {
            $providers[] = [
                'identifier' => self::PROVIDER_GOOGLE,
                'name' => 'Google',
                'redirect' => '/connect/google?target=' . $this->getTarget(),
                'icon' => 'fa fa-google',
            ];
        }
        if (in_array(self::PROVIDER_FACEBOOK, $enabled)) {
            $providers[] =
                [
                    'identifier' => self::PROVIDER_FACEBOOK,
                    'name' => 'Facebook',
                    'redirect' => '/connect/facebook?target=' . $this->getTarget(),
                    'icon' => 'fa fa-facebook',
                ];
        }
        if (in_array(self::PROVIDER_GITHUB, $enabled)) {
            $providers[] = [
                'identifier' => self::PROVIDER_GITHUB,
                'name' => 'Github',
                'redirect' => '/connect/github?target=' . $this->getTarget(),
                'icon' => 'fa fa-github',
            ];
        }
        if (in_array(self::PROVIDER_INSTAGRAM, $enabled)) {
            $providers[] =
                [
                    'identifier' => self::PROVIDER_INSTAGRAM,
                    'name' => 'Instagram',
                    'redirect' => '/connect/instagram?target=' . $this->getTarget(),
                    'icon' => 'fa fa-instagram',
                ];
        }

        return $providers;
    }
}
