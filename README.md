# Traefik middleware authenticator for SPID
[![pipeline status](https://gitlab.com/opencontent/traefik-spid-auth/badges/master/pipeline.svg)](https://gitlab.com/opencontent/traefik-spid-auth/-/commits/master)

## Come creare l'ambiente di prova

    docker-compose up -d

## Quali sono i file di configurazione rilevanti

I parameteri di `spid_symfony` si trovano in `app/config/config.yml`

Il file `spid-testenv2/sp_metadata.xml` contiene la configurazione del
service provider corrispondente a quanto scritto nel `config.yml`

## Come si usa

* si apre whoami.localtest.me
* si viene rediretti su auth.localtest.me
* non avendo una sessione spid valida si viene rediretti su /auth
* si sceglie l'ultimo provider disponibile
* si fa login su spid
* si torna a aith.localtest.me ma stavolta autenticati
* si DOVREBBE tornare all'app autenticati

## Indirizzi ambiente di prova

* [traefik dashboard](http://localtest.me)
* [api autenticata con spid](http://whoami.localtest.me)
* [servizio di autenticazione (SP)](http://auth.localtest.me)
* [Identity provider di test (IDP)](http://spid.localtest.me)

Tutti gli endpoint sono validi anche in https

## Risorse

Vedi
 
 - https://github.com/italia/spid-php-lib#configuring-and-installing
 - https://github.com/italia/spid-symfony-bundle/

## Configurazione di un Ente

### Configurare openlogin e un servizio di test protetto con forward-auth, per esempio una semplice API Come `containous/whoami`

### Test in ambiente di prova test.idp.gov.it

`openssl req -x509 -nodes -sha256 -days 365 -newkey rsa:2048 -subj "/C=IT/ST=Italy/L=Milan/O=myservice/CN=localhost" -keyout sp.key -out sp.crt`

share in container:
il certificato (file key e pem)
sp_10.xml

```
ENABLE_PROVIDERS: spid
SP_ENTITYID: 'https://login.comune.XXXXXXX.PP.it'
SP_SINGLELOGOUT: 'https://login.comune.XXXXXXX.PP.it/slo'
SP_ACS: 'https://login.comune.XXXXXXX.PP.it/acs'
TEST_SPID_IDP: 'enabled'
TEST_SPID_IDP_ID: 'https://idp.spid.gov.it'
TEST_SPID_IDP_NAME: 'Test IdP'
```

Al primo utilizzo se compare l'errore Target not found quando aprite il servizio di test significa che l'app symfony di autenticazione non riceve l'header `referer` dal chiamante e può dipendere da due motivi:
- lo state chiamando direttamente l'app symfony, che invece deve essere chiamata da traefik quando avviene la forward authentication
- non state passando l'header tramite traefik, verificate di aver configurato correttamente il middleware:
```
    - "--entryPoints.web.forwardedHeaders.trustedIPs=127.0.0.1/32,172.31.0.0/16,10.0.0.0/8"
    - "--entryPoints.websecure.forwardedHeaders.trustedIPs=127.0.0.1/32,172.31.0.0/16,10.0.0.0/8"
```

Se compare la pagina di login di SPID di test e quando fate click sull'ultimo provider in basso a dx ricevete l'errore 
> "L'entity ID "https://login.XXXXXXX.it" indicato nell'elemento <Issuer> non corrisponde a nessun Service Provider registrato in questo Identity Provider di test.
  
dovete registrare nell'IDP il metadata all'indirizzo:

https://idp.spid.gov.it/admin/databasesprecord/

Creare un record in quel database con l'Entity ID Configurato e incollare tutto il contenuto dei metadata del SP: https://login.XXXXXXX.it/metadata

A questo punto il vostro test dovrebbe andare a buon fine
  
### Test in ambiente di validazione

Cambiare la conf del 'autenticatore:
  
    TEST_SPID_IDP_ID: 'https://validator.spid.gov.it'
    TEST_SPID_IDP_NAME: 'SPID Validator'
    TEST_SPID_IDP_METADATA_NUMBER: 10

Scrivere i metadata dell'IDP di validazione reperibili all'indirizzo https://validator.spid.gov.it/metadata.xml nel file `sp_10.xml`
  
A questo punto si è pronti per lo step 5 dell'accreditamento:

https://www.spid.gov.it/come-diventare-fornitore-di-servizi-pubblici-e-privati-con-spid
